#!/bin/bash

MEMORY_FILE='memory.txt'
REGISTER_FILE='register.txt'
OUTPUT_FILE='saida.txt'
USAGE='Use $CODE [Arquivo de entrada] [Modo de operação] [Modo de saída].\n\n
    [Modo de operação] \tUse I para indireto ou D para direto.\n
    [Modo de saída] \tUse b, d ou h para binário, decimal ou hexadecimal respectivamente.\n'
MEMORY_ERROR='Atenção: Arquivo $CODE não indica início de memória. Consideramos 0x0.'

function setup(){
    CODE=$1
    MODE=$2
    FORMAT=$3
    NINSTR=0
    TIMEINSTR=0
 
    if [ -z "$CODE" ]||[ -z "$MODE" ]||[ -z "$FORMAT" ]; then
        echo -e $USAGE | sed -e "s/\$CODE/$CODE/g" 
        exit 85 #85: Sem argumentos
    fi
    
    if [[ ! $MODE =~ ^[ID]$ || ! $FORMAT =~ ^[bdh]$ ]]; then
        echo -e $USAGE | sed -e "s/\$CODE/$CODE/g" 
        exit 85
    fi

    read MEMORY <<< $(awk 'NR==1' $CODE)
    if [[ ! $MEMORY =~ ^([0-9]|[A-F])+$ ]]; then
        echo $MEMORY_ERROR | sed -e "s/\$CODE/$CODE/g"
        MEMORY="0" #(hexadecimal)
    fi
    MEMORY=$( hex2dec $MEMORY )
    PC=$MEMORY

    if [ $MODE == "I" ]; then
        OUTPUT_FILE='/dev/stdout'    
    fi

    #Limpando saída
    echo -n "" > $OUTPUT_FILE
}

function reg(){
    case $1 in
        't0') echo 8 ;;
        't1') echo 9 ;;
        't2') echo 10 ;;
        't3') echo 11 ;;
        't4') echo 12 ;;
        't5') echo 13 ;;
        't6') echo 14 ;;
        't7') echo 15 ;;
        's0') echo 16 ;; 
        's1') echo 17 ;;
        's2') echo 18 ;;
        's3') echo 19 ;;
        's4') echo 20 ;;
        's5') echo 21 ;;
        's6') echo 22 ;;
        's7') echo 23 ;;
        *) echo 0 ;;
    esac
}

function man(){
    local file
    local line
    if [ $1 == "register" ]; then
        file=$REGISTER_FILE
        line=$( reg $3 )
    else
        file=$MEMORY_FILE
        line=$( dec2hex $3 )
    fi

    if [ $2 == "get" ]; then
        bin2dec $( sed -ne "s/^$line \(.*\)\$/\1/p" $file )
    else
        sed -i -e "s/^\($line \).*\$/\1$(dec2bin $4)/" $file
    fi
}

function hex2dec(){
    echo "obase=10; ibase=16; ${1/$'\r'/}" | bc
}

function dec2hex(){
    echo "obase=16; ibase=10; ${1/$'\r'/}" | bc
}

function dec2bin(){
    if [ $1 -lt 0 ]; then
        echo "obase=2; ibase=10; $(( 2**32 + ${1/$'\r'/}))" | bc
    else
        local numb=$( echo "obase=2; ibase=10; ${1/$'\r'/}" | bc )
        local temp=$( printf "%32s" "$numb")
        echo ${temp// /0}
    fi
}

function bin2dec(){
    local bin=${1/$'\r'/}
    local sign=${bin:0:1}
    local bin=${bin:1}
    local dec=$((2#$bin))
    if [[ $sign == 1 ]]; then
        dec=$(($dec-(2**31)))
    fi
    echo $dec
}

function format(){
    case $FORMAT in
        'b')dec2bin $1;;
        'h')dec2hex $1;;
        *) echo $1;;
    esac
}

function printi(){
    #1: opcode
    #2: t
    #3: i
    #4: s

    local t=$(dec2bin $(reg $2));
    local i=$(dec2bin $3);
    local s=$(dec2bin $(reg $4));
    local instruction="$1${s:27}${t:27}${i:16}"
    echo -e "I[31-0]\t\t$instruction" >> $OUTPUT_FILE
    echo -e "I[25-21]\t${instruction:6:5}" >> $OUTPUT_FILE
    echo -e "I[20-16]\t${instruction:11:5}" >> $OUTPUT_FILE
    echo -e "I[15-11]\t${instruction:16:5}" >> $OUTPUT_FILE
    echo -e "I[15-0]\t\t${instruction:16:16}" >> $OUTPUT_FILE
}

function printr(){
    #1: function
    #2: d
    #3: s
    #4: t

    local d=$(dec2bin $(reg $2));
    local s=$(dec2bin $(reg $3));
    local t=$(dec2bin $(reg $4));
    local instruction="000000${s:27}${t:27}${d:27}00000$1"
    echo -e "I[31-0]\t\t$instruction" >> $OUTPUT_FILE
    echo -e "I[25-21]\t${instruction:6:5}" >> $OUTPUT_FILE
    echo -e "I[20-16]\t${instruction:11:5}" >> $OUTPUT_FILE
    echo -e "I[15-11]\t${instruction:16:5}" >> $OUTPUT_FILE
    echo -e "I[15-0]\t\t${instruction:16:16}" >> $OUTPUT_FILE
}

function logr(){
    sleep "0.000000008"
    echo -e 'PCWriteCond\t0\nPCWrite\t\t0\nIorD\t\t0\nMemRead\t\t0
MemToReg\t0\nIRWrite\t\t1\nPCSrc\t\t0\nALUOp\t\t10\nALUSrcB\t\t00\nALUSrcA\t\t1\nRegWrite\t1
RegDst\t\t0\nTempInstr\t8ns\n' >> $OUTPUT_FILE
    TIMEINSTR=$(( $TIMEINSTR + 8 ))
}

function execute(){  
    echo -e "PC\t\t$(format "$PC")" >> $OUTPUT_FILE   #OUTPUT: PC
    NINSTR=$(( $NINSTR + 1 ))
    case $1 in
        'lw')
            local address=$(( $( man register get $4 ) + $3 + $MEMORY))
            local ReadData=$(man memory get $address )
            man register set $2 $ReadData
            printi "100011" "$2" "$3" "$4"
            echo -e "ALUResult\t$(format $address)\nMemData\t\t$(format $ReadData)
PCWriteCond\t0\nPCWrite\t\t0\nIorD\t\t1\nMemRead\t\t1\nMemWrite\t0
MemToReg\t1\nIRWrite\t\t1\nPCSrc\t\t0\nALUOp\t\t00\nALUSrcB\t\t10
ALUSrcA\t\t1\nRegWrite\t1\nRegDst\t\t0\nTempoInstr\t12ns\n" >> $OUTPUT_FILE
            sleep $(echo "12/(10^9)" | bc -l)
            TIMEINSTR=$(( $TIMEINSTR + 12 ))
            ;;
        'sw')
            local address=$(( $( man register get $4 ) + $3 + $MEMORY))
            local WriteData=$(man register get $2)
            man memory set $address $WriteData
            printi "101011" "$2" "$3" "$4"
            echo -e "ALUResult\t$(format $address)\nWriteData\t$(format $WriteData)
PCWriteCond\t0\nPCWrite\t\t0\nIorD\t\t1\nMemRead\t\t0\nMemWrite\t1
MemToReg\t0\nIRWrite\t\t1\nPCSrc\t\t0\nALUOp\t\t00\nALUSrcB\t\t10
ALUSrcA\t\t1\nRegWrite\t1\nRegDst\t\t0\nTempoInstr\t12ns\n" >> $OUTPUT_FILE
            sleep $(echo "11/(10^9)" | bc -l)
            TIMEINSTR=$(( $TIMEINSTR + 11 ))
            ;;
        'add')
            local a=$( man register get $3 )
            local b=$( man register get $4 )
            man register set $2 $(( a+b ))
            printr "100000" $2 $3 $4
            echo -e "ReadData1\t$(format $a)\nReadData2\t$(format $b)
AluResult\t$(format $(man register get $2))" >> $OUTPUT_FILE
            logr
            ;;
        'sub')
            local a=$( man register get $3 )
            local b=$( man register get $4 )
            man register set $2 $(( a-b ))
            printr "100010" $2 $3 $4
            echo -e "ReadData1\t$(format $a)\nReadData2\t$(format $b)
AluResult\t$(format $(man register get $2))" >> $OUTPUT_FILE
            logr
            ;;
        'and')
            local a=$( dec2bin $( man register get $3 ))
            local b=$( dec2bin $(man register get $4 ))
            local result=""
            for i in {1..31}; do
                result="$result$(( ${a:$i:1} && ${b:$i:1} ))"
            done
            man register set $2 $( bin2dec $result )
            printr "100100" $2 $3 $4
            echo -e "ReadData1\t$(format $a)\nReadData2\t$(format $b)
AluResult\t$(format $(man register get $2))" >> $OUTPUT_FILE
            logr
            ;;
        'or')
            local a=$( dec2bin $( man register get $3 ))
            local b=$( dec2bin $(man register get $4 ))
            local result=""
            for i in {1..31}; do
                result="$result$(( ${a:$i:1} || ${b:$i:1} ))"
            done
            man register set $2 $( bin2dec $result )
            printr "100101" $2 $3 $4 
            echo -e "ReadData1\t$(format $a)\nReadData2\t$(format $b)
AluResult\t$(format $(man register get $2))" >> $OUTPUT_FILE
            logr
            ;;
        'slt')
            local a=$( man register get $3 )
            local b=$( man register get $4 )
            local result=0
            if [ $a -lt $b ]; then
                result=1
            fi
            man register set $2 $result
            printr "101010" $2 $3 $4 
            echo -e "ReadData1\t$(format $a)\nReadData2\t$(format $b)
AluResult\t$(format $(man register get $2))" >> $OUTPUT_FILE
            logr
            ;;
    esac
}

function decode(){
    local immediate='^([a-z]+) ?\$(..) ?, ?([0-9].*) ?\(\$(..)\)' 
    local register='^([a-z]+) ?\$(..) ?, ?\$(..) ?, ?\$(..)' 
    if [[ $1 =~ $immediate || $1 =~ $register ]]; then 
        execute ${BASH_REMATCH[1]} ${BASH_REMATCH[2]} ${BASH_REMATCH[3]} ${BASH_REMATCH[4]}
        PC=$(( $PC + 4 ))
    fi
}

setup "$1" $2 $3

while read -u3 instruction; do
    decode "$instruction"
    if [[ $MODE == "I" ]]; then
        read -p "Pressione [Enter] para continuar" 
    fi
done 3< $CODE

echo -e "TempoTotal\t"$TIMEINSTR"ns" >> $OUTPUT_FILE
echo -e "QuantInstr\t$NINSTR" >> $OUTPUT_FILE
